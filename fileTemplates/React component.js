(function() {
    "use strict";

    const React = require("react");
    
    class ${NAME} extends React.Component {
        render() {
            return React.createElement("div");
        }
    }

    module.exports = ${NAME};
})();

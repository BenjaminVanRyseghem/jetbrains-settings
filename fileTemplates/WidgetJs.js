define([
	'widgetjs/core'
],function(widgetjs) {

	/**
	 * `${NAME}` is 
	 * @param {*} spec
	 * @param {*} my
	 * @return {{}}
	 */
	function ${NAME}(spec, my){
		spec = spec || {};
		my = my || {};

		var that = widgetjs.widget(spec, my);

		//
		// Rendering
		//

		that.renderContentOn = function(html) {

		};
		
		return that;
	}
    
    return ${NAME};
});